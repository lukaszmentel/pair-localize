module readFilesModule
 use typesModule
 implicit none 

 private

 type datafile 
    character(len=100)    :: input
    character(len=100)    :: dat_file 
    character(len=100)    :: orbital_file 
    integer(ik)           :: unt=11
    integer(ik)           :: numMos
    integer(ik)           :: numAos
    integer(ik)           :: homo
    integer(ik)           :: fzc
    real(dp), allocatable :: nos(:) 
    real(dp), allocatable :: orbitals(:,:) 
 end type

 interface new
    module procedure new
 end interface

 interface delete
    module procedure delete
 end interface

 interface getons
    module procedure getnoons 
 end interface 

 interface printons
    module procedure printnoons 
 end interface 

 interface getOrbitals
    module procedure read_gamess_vectors 
 end interface

 interface getHFMOs
    module procedure read_gamess_hfmos 
 end interface

 interface getNOs
    module procedure read_gamess_nos 
 end interface

 public datafile, new, delete, getons, printons, getOrbitals, getHFMOs, getNOs

contains

 subroutine new(self)
  type(datafile)     :: self
  character(len=100) :: buffer, input_filename, vec_filename, orbital_file
  character(len=10)  :: dummy 
  integer(ik)        :: m, n_mos, n_aos, homo, fzc
  namelist /input/ vec_filename, n_mos, n_aos, homo, fzc, orbital_file
 
  ! get the dat filename from the command line 
  !     .dat file should have the 2-el integrals and 2RDM elements
  ! if not provided an error will occure  

  call getarg(1,buffer)
  read(buffer,*) input_filename
  self%input = input_filename
  open(unit=12, file=input_filename,status="old",form="formatted",delim='apostrophe')
  read(12,nml=input)
  close(12)
  self%orbital_file = orbital_file
  self%numMos = n_mos
  self%numAos = n_aos
  self%homo   = homo
  self%fzc    = fzc
  self%dat_file  = vec_filename


  print *, "numMos  = ", self%numMos," homo    = ", self%homo 
  print *, "fzc     = ", self%fzc

  allocate(self%nos(self%numMos))
  allocate(self%orbitals(self%numAos,self%numMos))
  self%nos      = 0.0_dp
  self%orbitals = 0.0_dp
 end subroutine new

 subroutine getNOONs(self)
  type(datafile)               :: self
  character(len=20), parameter :: noFmt = '(5(f16.10))'
  character(len=100)           :: line
  integer(ik)                  :: i,io,n

  n = int(self%numMos/5)
  rewind(self%unt)
  do 
      read(self%unt,*,iostat=io) line
      if (io < 0) exit
      if (trim(line) == "$OCC" .or. trim(line) == "$OCCNO") then
          do i = 1, n-1
              read(self%unt,noFmt) self%nos(i*5-4:i*5)
          enddo
          read(self%unt,noFmt) self%nos(n*5-4:self%numMos)
       exit    
      endif 
  enddo
 end subroutine getNOONs

 subroutine printNOONs(self)
  type(datafile) :: self
  integer(ik)    :: i
  print '(/a/)', 'NO Occupation Numbers' 
  do i = 1, self%numMos
     print '(i4,2x,f16.10)', i, self%nos(i)
  enddo
 end subroutine printNOONs 

 subroutine delete(self)
  type(datafile) :: self
 
  close(self%unt)
  if (allocated(self%nos))   deallocate(self%nos) 
  if (allocated(self%orbitals))   deallocate(self%orbitals) 

 end subroutine delete

 subroutine read_gamess_vectors(self)
  type(datafile)      :: self
  character(len=100)  :: line
  integer(ik)         :: io, i, j, l, ilab, llab, nlines

  open(unit=14,file=self%dat_file,form='formatted',status='old') 
  rewind(14)

 ! find the $VEC section of the dat file 
  do 
    read(14, plain ,iostat=io) line
    if (io < 0) then
        print *, 'last line...'
        exit
    endif
   ! if (index(line,'$VEC') > 0) then           ! original line if only one $VEC provided
    if (index(line,'STARTING ORBITALS') > 0) then 
        print *, line
        exit
    endif 
  enddo 
 !advance one line (containing $VEC)
  read(14,*)

 ! calculate the number of lines per orbital
  if (mod(self%numMos,5) == 0) then
    nlines = int(self%numMos/5)
  else
    nlines = int(self%numMos/5,ik)+1  
  endif
 
  print *, 'nlines = ', nlines

 ! read the orbitals
  do i = 1, self%numMos
    if (i >= 100) then 
      ilab = mod(i, 100)
    else 
      ilab = i
    endif
    do l = 1, nlines
      if (l > 1000) then 
        llab = mod(l, 1000)
      else
        llab = l
      endif
      if (l < nlines) then
        read(14,gmsvfmt) ilab,llab,(self%orbitals(j+(l-1)*5,i),j=1,5)
      else
        read(14,gmsvfmt) ilab,llab,(self%orbitals(j+(l-1)*5,i),j=1,5-(l*5-self%numAos))
      endif
    enddo
  enddo
  close(14) 

 end subroutine read_gamess_vectors

 subroutine read_gamess_hfmos(self)
  type(datafile)      :: self
  character(len=100)  :: line
  integer(ik)         :: io, i, j, l, ilab, llab, nlines

  open(unit=14,file=self%dat_file,form='formatted',status='old') 
  rewind(14)

 ! find the $VEC section of the dat file 
  do 
    read(14, plain ,iostat=io) line
    if (io < 0) then
        print *, 'last line...'
        exit
    endif
   ! if (index(line,'$VEC') > 0) then           ! original line if only one $VEC provided
    if (index(line,'--- CLOSED SHELL ORBITALS ---') > 0) then 
        print *, line
        exit
    endif 
  enddo 
 !advance three lines (containing title, energies and $VEC)
  read(14,*)
  read(14,*)
  read(14,*)

 ! calculate the number of lines per orbital
  if (mod(self%numAos,5) == 0) then
    nlines = int(self%numAos/5)
  else
    nlines = int(self%numAos/5,ik)+1  
  endif
 
  print *, 'nlines = ', nlines

 ! read the orbitals
  do i = 1, self%numMos
    if (i >= 100) then 
      ilab = mod(i, 100)
    else 
      ilab = i
    endif
    do l = 1, nlines
      if (l > 1000) then 
        llab = mod(l, 1000)
      else
        llab = l
      endif
      if (l < nlines) then
        read(14,gmsvfmt) ilab,llab,(self%orbitals(j+(l-1)*5,i),j=1,5)
      else
        read(14,gmsvfmt) ilab,llab,(self%orbitals(j+(l-1)*5,i),j=1,5-(l*5-self%numAos))
      endif
    enddo
  enddo
  close(14) 

 end subroutine read_gamess_hfmos

 subroutine read_gamess_nos(self)
  type(datafile)      :: self
  character(len=100)  :: line
  integer(ik)         :: io, i, j, l, ilab, llab, nlines

  open(unit=14,file=self%dat_file,form='formatted',status='old') 
  rewind(14)

 ! find the $VEC section of the dat file 
  do 
    read(14, plain ,iostat=io) line
    if (io < 0) then
        print *, 'last line...'
        exit
    endif
    if (index(line,'GUGA-CI NO-S') > 0) then 
        print *, line
        exit
    endif 
  enddo 
  do 
    read(14, plain ,iostat=io) line
    if (io < 0) then
        print *, 'last line...'
        exit
    endif
    if (index(line,'$VEC') > 0) then          
        print *, line
        exit
    endif 
  enddo 
 !advance three lines (containing title, energies and $VEC)

 ! calculate the number of lines per orbital
  if (mod(self%numMos,5) == 0) then
    nlines = int(self%numMos/5)
  else
    nlines = int(self%numMos/5,ik)+1  
  endif
 
  print *, 'nlines = ', nlines

 ! read the orbitals
  do i = 1, self%numMos
    if (i >= 100) then 
      ilab = mod(i, 100)
    else 
      ilab = i
    endif
    do l = 1, nlines
      if (l > 1000) then 
        llab = mod(l, 1000)
      else
        llab = l
      endif
      if (l < nlines) then
        read(14,gmsvfmt) ilab,llab,(self%orbitals(j+(l-1)*5,i),j=1,5)
      else
        read(14,gmsvfmt) ilab,llab,(self%orbitals(j+(l-1)*5,i),j=1,5-(l*5-self%numAos))
      endif
    enddo
  enddo
  close(14) 

 end subroutine read_gamess_nos

end module readFilesModule
