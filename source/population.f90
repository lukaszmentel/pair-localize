module populationModule
 use typesModule
 implicit none

 private
 
 type fragment
    integer(ik) :: numAos
    integer(ik) :: ao_ind_min
    integer(ik) :: ao_ind_max
    integer(ik), allocatable :: occupied(:)
    integer(ik), allocatable :: virtual(:)
 end type
 
 type population
    integer(ik)                 :: n_fragments
    integer(ik)                 :: n_mos
    integer(ik)                 :: fzc
    integer(ik)                 :: homo
    type(fragment), allocatable :: fragments(:)   
    real(dp),       allocatable :: pops(:,:)
 end type

 interface new
    module procedure new
 end interface 

 interface delete
    module procedure delete
 end interface 

 interface analyze
    module procedure analyze
 end interface 

 public :: population, analyze, new, delete

 contains

 subroutine new(self, nmo, input_filename)
  type(population) :: self
  character(len=100), intent(in) :: input_filename
  integer(ik)      :: i, nmo, nfrags, imin, imax
  integer(ik)      :: n_aos_f1, n_aos_f2, ao_min_f1, ao_max_f1, ao_min_f2, ao_max_f2, fzc, homo
  namelist /fragments/ n_aos_f1, n_aos_f2, ao_min_f1, ao_max_f1, ao_min_f2, ao_max_f2, fzc, homo

  open(unit=12, file=input_filename,status="old",form="formatted",delim='apostrophe')
  read(12,nml=fragments)
  close(12)
  self%n_mos = nmo
 
  self%n_fragments = 2

  if (.not. allocated(self%fragments)) allocate(self%fragments(self%n_fragments)) 
  if (.not. allocated(self%pops)) allocate(self%pops(nmo,self%n_fragments))

  !do i = 1, self%n_fragments
  !  print *, 'give imin and imax for fragment ',i
  !  read *, imin, imax
    self%fragments(1)%numAos     = n_aos_f1
    self%fragments(1)%ao_ind_min = ao_min_f1
    self%fragments(1)%ao_ind_max = ao_max_f1
    self%fragments(2)%numAos     = n_aos_f2
    self%fragments(2)%ao_ind_min = ao_min_f2
    self%fragments(2)%ao_ind_max = ao_max_f2
    self%fzc  = fzc
    self%homo = homo
  !enddo
  if (.not. allocated(self%fragments(1)%occupied)) allocate(self%fragments(1)%occupied((self%homo-self%fzc)/2))
  if (.not. allocated(self%fragments(1)%virtual))  allocate(self%fragments(1)%virtual(self%fragments(1)%numAos-self%homo/2))
  if (.not. allocated(self%fragments(2)%occupied)) allocate(self%fragments(2)%occupied((self%homo-self%fzc)/2))
  if (.not. allocated(self%fragments(2)%virtual))  allocate(self%fragments(2)%virtual(self%fragments(2)%numAos-self%homo/2))

 end subroutine new

 subroutine delete(self)
  type(population) :: self

  if (allocated(self%fragments(1)%occupied)) deallocate(self%fragments(1)%occupied)
  if (allocated(self%fragments(1)%virtual)) deallocate(self%fragments(1)%virtual)
  if (allocated(self%fragments(2)%occupied)) deallocate(self%fragments(2)%occupied)
  if (allocated(self%fragments(2)%virtual)) deallocate(self%fragments(2)%virtual)
  if (allocated(self%pops)) deallocate(self%pops)
  if (allocated(self%fragments)) deallocate(self%fragments) 
 end subroutine delete

 subroutine analyze(self, coeffs)
  type(population) :: self
  real(dp)         :: coeffs(:,:)
  integer(ik)      :: i, j, k, f, l, m
  real(dp)         :: pop
  
  do i = 1, size(coeffs,2)
    do f = 1, self%n_fragments
      pop = 0.0_dp
      do j = self%fragments(f)%ao_ind_min, self%fragments(f)%ao_ind_max
        pop = pop + coeffs(j,i)*coeffs(j,i)
      enddo
      self%pops(i,f) = pop
    enddo
  enddo

  write(*,'(/4x,a/4x,2i15)') 'AO populations', 1, 2   
  do i = 1, self%n_mos
    write(*,'(i4,2f15.5)') i, (self%pops(i,j),j=1,self%n_fragments) 
  enddo

!  j = 0; k = 0; l = 0; m = 0 
!  do i = 1,self%n_mos
!    if (i > self%fzc .and. i <= self%homo ) then
!        if (self%pops(i,1) > self%pops(i,2)) then
!            j = j + 1
!            self%fragments(1)%occupied(j) = i
!        else
!            k = k + 1
!            self%fragments(2)%occupied(k) = i
!        endif
!    elseif (i > self%homo ) then 
!        if (self%pops(i,1) > self%pops(i,2)) then
!            l = l + 1
!            self%fragments(1)%virtual(l) = i
!        else
!            m = m + 1
!            self%fragments(2)%virtual(m) = i
!        endif
!    endif 
!  enddo
!  
!  write(*,'(/a/)') 'orbital label localized on distinct centers'
!  write(*,'(5x,2i5)') 1,2
!  write(*,'(/a/)') 'occupied'
!  do i = 1, size(self%fragments(1)%occupied)
!    write(*,'(i3,2x,2i5)') i, (self%fragments(j)%occupied(i),j=1,self%n_fragments)
!  enddo
!  write(*,'(/a/)') 'virtual'
!  do i = 1, size(self%fragments(1)%virtual)
!    write(*,'(i3,2x,2i5)') i, (self%fragments(j)%virtual(i),j=1,self%n_fragments)
!  enddo
 end subroutine analyze 

end module populationModule
