module LocalizationModule
 use TypesModule
 use readFilesModule
 implicit none 

 contains 

 subroutine localize_pairs(dat, loc_orbitals)
  type(datafile), intent(in)    :: dat 
  real(dp),      intent(inout) :: loc_orbitals(:,:)
  integer(ik),   allocatable   :: pairs(:,:)
  integer(ik) :: i,j,io
  namelist /pair/ pairs 
 
  allocate(pairs(size(dat%orbitals,2)/2,2))

  open(unit=12, file=dat%orbital_file,status="old",form="formatted",delim='apostrophe')

  do i = 1, size(dat%orbitals,2)/2
    read(12, '(2i5)',iostat=io) pairs(i,1), pairs(i,2)
  enddo

  close(12)

  write(*,'(/a/)') 'pair from the input'
  do i = 1, size(pairs,1)
    write(*,'(2i5)') (pairs(i,j),j=1,2)
  enddo

! copy frozen core orbitals without localization
  do i = 1, dat%fzc
    loc_orbitals(:,i) = dat%orbitals(:,i)
  enddo 

  do i = 1_ik + dat%fzc/2_ik, size(pairs,1)
    do j = 1_ik + dat%fzc, size(dat%orbitals,1)
      loc_orbitals(j,2*i-1) = (dat%orbitals(j,pairs(i,1)) + dat%orbitals(j,pairs(i,2)))/sqrt(2.0_dp)
      loc_orbitals(j,2*i)   = (dat%orbitals(j,pairs(i,1)) - dat%orbitals(j,pairs(i,2)))/sqrt(2.0_dp)
    enddo 
  enddo
  
  deallocate(pairs)
   
 end subroutine localize_pairs

end module LocalizationModule
