module typesModule
 implicit none 

 integer, parameter :: ik = 4
 integer, parameter :: dp = kind(1.0d0)

 character(len=20), parameter :: plain   = '(a100)' 
 character(len=20), parameter :: mofmt   = '(i6,es22.12)' 
 character(len=20), parameter :: orbefmt = '(a4,f10.6)' 
 character(len=20), parameter :: gmsvfmt = '(i2,i3,5(es15.8))' 

end module typesModule
