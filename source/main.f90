program noFunctionals
 use typesModule 
 use readFilesModule
 use formattedOutputModule
 use populationModule
 use LocalizationModule
 implicit none
 type(datafile)   :: dat
 type(population) :: pop, pop_loc
 real(dp)         :: e
 real(dp), allocatable :: loc_orbitals(:,:) 

 call new(dat)
 call new(pop,dat%numMos,dat%input)
 call getHFMOs(dat)
! call getOrbitals(dat)
! call getNOs(dat)
 write(*,'(/5x,a/)') 'Natural Orbitals'
 call matprint(dat%orbitals)
 call analyze(pop,dat%orbitals)
 allocate(loc_orbitals(size(dat%orbitals,1),size(dat%orbitals,2)))
 call localize_pairs(dat, loc_orbitals)
 call new(pop_loc,dat%numMos,dat%input)
 write(*,'(/5x,a/)') 'Pair localized HF Orbitals'
 call matprint(loc_orbitals)
 call analyze(pop_loc,loc_orbitals)
 call print_gamess_vecs(loc_orbitals)
 deallocate(loc_orbitals)

 call delete(dat)

end program noFunctionals
